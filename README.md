# Database Administration 
## Environment setup
### Setting Up Northwind Database with Docker and SQL Server

#### Install Dependencies

**Install `mssql-tools`**

- Debian

    ```bash
    sudo apt install mssql-tools
    ```
- Fedora
    ```bash
    sudo dnf install mssql-tools
    ```
- openSUSE
    ```bash
    sudo zypper install mssql-tools
    ```
- Gentoo
    ```bash
    sudo emerge -av dev-db/mssql-tools

    ```
- Arch Linux
    ```bash
    sudo pacman -S mssql-tools
    ```

**Install Docker**

### Install Docker:

#### For Linux Users:

1. **Ubuntu/Debian**:
   
   You can install Docker on Ubuntu/Debian by following the official instructions from Docker: [Install Docker Engine on Ubuntu](https://docs.docker.com/engine/install/ubuntu/).

2. **Fedora**:
   
   Follow the official Docker documentation for Fedora: [Install Docker Engine on Fedora](https://docs.docker.com/engine/install/fedora/).

3. **openSUSE**:

   Refer to the Docker documentation for openSUSE: [Install Docker Engine on openSUSE](https://docs.docker.com/engine/install/opensuse/).

4. **Arch Linux**:

   You can install Docker on Arch Linux using the official Docker package from the Arch User Repository (AUR). You can find instructions on the Arch Wiki: [Docker - ArchWiki](https://wiki.archlinux.org/title/Docker).

5. **Other Distributions**:

   For other Linux distributions, you can refer to the official Docker documentation for generic Linux installations: [Install Docker Engine](https://docs.docker.com/engine/install/).

Follow the appropriate instructions for your Linux distribution to install Docker. Once installed, you can proceed with pulling the MSSQL Server image and running the Docker container as previously described.

## MSSQL Server container with Northwind

### Pull the MSSQL Server Image

```bash
    docker pull mcr.microsoft.com/mssql/server
```
### Get Microsoft Official Northwind query

```bash
wget https://github.com/microsoft/sql-server-samples/blob/master/samples/databases/northwind-pubs/instnwnd.sql 
```
### Run the Container

```bash
docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=<your_password>" -p 1433:1433 --name sqlserver -d mcr.microsoft.com/mssql/server:latest
```


#### Connect to SQL Server

```bash
docker exec -it sqlserver /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P "<your_password>"
```

#### Run the SQL Script

```sql
USE master;
GO
:r /path/to/instwnd.sql;
GO
```

Replace `/path/to/instwnd.sql` with the actual path to the `instwnd.sql` script file on your local machine.

## Install on Microsoft Windows 
1. **Download SQL Server**:
   - Go to the [Microsoft SQL Server Downloads](https://www.microsoft.com/en-us/sql-server/sql-server-downloads) page.
   - Choose the edition of SQL Server you want to install.
   - Click on the download link and follow the on-screen instructions to download the installer.

2. **Follow Installation Instructions**:
   - Once the download is complete, double-click on the downloaded file to run the installer.
   - Follow the on-screen instructions to complete the installation process.

# SQL Queries

Queries are located in mmsql directory, to run them, copy the script on the docker container
```bash
docker cp mssql/data_queries.sql sqlserver:/tmp
docker exec -ti sqlserver /opt/mssql-tools/bin/sqlcmd -S -P <password> -i /tmp/data_queries.sql 
```

or run the script using the Data-base Manager on Windows


# Module 11, Algorithm

The `Module11CheckDigit` program calculates the check digit of a given numeric input using the Module 11 algorithm. This check digit algorithm is commonly used in various applications, such as validating identification numbers, credit card numbers, and barcodes.

#### Usage
To execute the program, run the following command:
```bash
dotnet run Module11CheckDigit <number>
```
Replace `<number>` with the numeric input for which you want to calculate the check digit.
### Explanation of csproj file

The `Module11CheckDigit.csproj` file is a project file for managing the compilation and build settings of the `Module11CheckDigit` program. Below is a breakdown of its contents:

```xml
<Project Sdk="Microsoft.NET.Sdk">
<PropertyGroup>
  <TargetFramework>net5.0</TargetFramework>
  <EnableDefaultCompileItems>false</EnableDefaultCompileItems>
  <OutputType>Exe</OutputType>
</PropertyGroup>
```

Specifies the SDK used for building the project. In this case, it's the Microsoft .NET SDK.

Defines project-specific properties:

- `<TargetFramework>`: Specifies the target framework version. Here, it's set to `.NET 5.0`.
- `<EnableDefaultCompileItems>`: Disables the default compilation of files in the project directory.
- `<OutputType>`: Specifies the type of output generated by the project. Here, it's set to produce an executable (`Exe`).

```xml
<ItemGroup>
  <Compile Include="Module11CheckDigit.cs" />
</ItemGroup>
```

### Execution on GNU/Linux

1. Make sure you have the .NET SDK installed on your system.
2. Navigate to the directory containing `Module11CheckDigit.csproj`.
3. Run the following command to build and execute the program:
    ```bash
    dotnet run Module11CheckDigit <number>
    ```
    Replace <number> with the numeric input for which you want to calculate the check digit.
    
### Execution on Windows

1. Ensure you have the .NET SDK installed on your system.
2. Open Command Prompt or PowerShell.
3. Navigate to the directory containing `Module11CheckDigit.csproj`.
4. Run the following command to build and execute the program:
   ```bash
   dotnet run Module11CheckDigit <number>
   ```
   Replace <number> with the numeric input for which you want to calculate the check digit.

# Power BI Report: Sales Analysis

This Power BI report provides insights into sales data from a sample dataset (replace with details about your data source). It allows you to explore sales trends, identify key performance indicators (KPIs), and gain valuable business intelligence.

### Report Sections

The report is structured into the following sections:

* **Data Transformation:** (Describe the transformations applied to the raw data, including handling missing values, formatting data types, etc.)
* **Data Model:** (Explain the relationships between tables in your model, highlighting any star schema or snowflake schema design.)
* **Metrics and KPIs:** (List and define the key metrics and KPIs calculated in the report, such as total sales, average order value, customer acquisition cost, etc.)
* **Visualizations:** (Enumerate the types of visualizations used in the report, such as bar charts, line charts, tables, and explain how they represent the data.)
* **Filters and Slicers:** (Detail the filters and slicers available in the report, explaining how users can interact with the data to focus on specific segments.)

**Example:**

* **Visualization:** Line chart
* **Purpose:** This line chart tracks sales trends over time, allowing users to identify seasonal patterns or growth trends.

**Additional Considerations:**

* (Mention any report drill-down capabilities, advanced calculations with DAX, or custom formatting applied.)

### Getting Started

To use this report effectively, you should be familiar with the following:

* Basic Power BI concepts (datasets, reports, visuals)
* (List any specific knowledge required to understand the report's metrics or calculations)

**Tips:**

* Use the filters and slicers to explore the data from different perspectives.
* Drill down into specific data points for more granularity.
* Export the report to Excel or other formats for further analysis.






