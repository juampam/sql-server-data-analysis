-- 1. Basic SQL Querys

-- a) EmployeeID = 6 && 9
SELECT o.OrderID, o.CustomerID, o.EmployeeID, CONVERT(varchar, o.OrderDate, 103) as OrderDate,
       o.ShipCountry, c.CompanyName, c.ContactName
FROM Orders o
JOIN Customers c ON o.CustomerID = c.CustomerID
WHERE o.EmployeeID IN (6, 9);

-- b) EmployeeID between 1 && 4
SELECT o.OrderID, o.CustomerID, o.EmployeeID, CONVERT(varchar, o.OrderDate, 103) as OrderDate,
       o.ShipCountry, c.CompanyName, c.ContactName
FROM Orders o
JOIN Customers c ON o.CustomerID = c.CustomerID
WHERE o.EmployeeID BETWEEN 1 AND 4;

-- c) Order date equals to 02/02/1998
SELECT o.OrderID, o.CustomerID, o.EmployeeID, CONVERT(varchar, o.OrderDate, 103) as OrderDate,
       o.ShipCountry, c.CompanyName, c.ContactName
FROM Orders o
JOIN Customers c ON o.CustomerID = c.CustomerID
WHERE CONVERT(varchar, o.OrderDate, 103) = '02/02/1998';


-- 2. Insert and Cursors

-- variable table
DECLARE @tempTable TABLE (
    OrderID INT,
    CustomerID VARCHAR(10),
    OrderDate DATE,
    ShipAddress VARCHAR(100),
    ShipCity VARCHAR(50)
);

-- variable declarations
DECLARE @CustomerID VARCHAR(10), @OrderDate DATE, @ShipAddress VARCHAR(100), @ShipCity VARCHAR(50);
DECLARE @OrderIdCounter INT = 10;

-- Orders select cursor

DECLARE cursorOrders CURSOR FOR
SELECT CustomerID, OrderDate, ShipAddress, ShipCity
FROM Orders;

OPEN cursorOrders;
FETCH NEXT FROM cursorOrders INTO @CustomerID, @OrderDate, @ShipAddress, @ShipCity;

WHILE @@FETCH_STATUS = 0
BEGIN
    INSERT INTO @tempTable (OrderID, CustomerID, OrderDate, ShipAddress, ShipCity)
    VALUES (@OrderIdCounter, @CustomerID, @OrderDate, @ShipAddress, @ShipCity);

    SET @OrderIdCounter += 5; -- Increment the OrderID counter

    FETCH NEXT FROM cursorOrders INTO @CustomerID, @OrderDate, @ShipAddress, @ShipCity;
END;

CLOSE cursorOrders;
DEALLOCATE cursorOrders;

-- the victe xD

DECLARE cursorOrders CURSOR FOR
SELECT CustomerID, OrderDate, ShipAddress, ShipCity
FROM Orders
WHERE CustomerID = 'VICTE';

-- select temp table

SELECT * FROM @tempTable;

-- 3. Triggers

-- Create a trigger in the Suppliers table
CREATE TRIGGER InsertIntoShippers
ON Suppliers
AFTER INSERT
AS
BEGIN
    SET NOCOUNT ON;

    -- Declare variables
    DECLARE @CompanyName NVARCHAR(100),
            @ContactName NVARCHAR(50),
            @ContactTitle NVARCHAR(50),
            @Address NVARCHAR(100),
            @City NVARCHAR(50),
            @Region NVARCHAR(50),
            @PostalCode NVARCHAR(20),
            @Country NVARCHAR(50),
            @Phone NVARCHAR(20),
            @Fax NVARCHAR(20),
            @HomePage NVARCHAR(100),
            @SupplierID INT;

    -- Retrieve inserted data
    SELECT @CompanyName = CompanyName,
           @ContactName = ContactName,
           @ContactTitle = ContactTitle,
           @Address = Address,
           @City = City,
           @Region = Region,
           @PostalCode = PostalCode,
           @Country = Country,
           @Phone = Phone,
           @Fax = Fax,
           @HomePage = HomePage,
           @SupplierID = SupplierID
    FROM inserted;

    -- Insert data into Shippers
    INSERT INTO Shippers (CompanyName, ContactName, ContactTitle, Address, City, Region, PostalCode, Country, Phone, Fax, HomePage, shipperid)
    VALUES (@CompanyName, @ContactName, @ContactTitle, @Address, @City, @Region, @PostalCode, @Country, @Phone, @Fax, @HomePage, @SupplierID);
END;

