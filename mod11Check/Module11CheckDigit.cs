using System;

class Module11CheckDigit
{
    static void Main(string[] args)
    {
        if (args.Length != 1)
        {
            Console.WriteLine("Usage: dotnet run Module11CheckDigit <number>");
            return;
        }

        string number = args[0];
        int[] weights = { 2, 3, 4, 5, 6, 7 };

        int sum = 0;
        int weightIndex = 0;
        for (int i = number.Length - 1; i >= 0; i--)
        {
            int digit = int.Parse(number[i].ToString());
            sum += digit * weights[weightIndex % weights.Length];
            weightIndex++;
        }

        int checkDigit = (11 - (sum % 11)) % 11;
        Console.WriteLine("Check Digit: " + (checkDigit < 10 ? checkDigit : (checkDigit == 10 ? 1 : 0)));
    }
}
